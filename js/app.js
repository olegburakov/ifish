$(document).ready(function() {

    $("#owl").owlCarousel({
        items: 1,
        singleItem : true,
    });

    $('.top-menu-button').click(function(e)
    {
        e.preventDefault();
        $('#popup-menu').arcticmodal();
    });

});